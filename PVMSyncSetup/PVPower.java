//based on “UDP Local Communication Interface for the PVM1010”
//modified by Alfred Cellier 2021/02/17
//scale factors herein are for PVP 4800
//compile with “javac PVPower.java” in Terminal

import java.io.*;
import java.net.*;
import java.util.*;
public class PVPower {
public static void main(String[] args) throws IOException {

//Required program startup code
if (args.length != 1) {
System.out.println("Usage: java Client <Host>");
return; }

//Step 1) Create the request packet
byte[] buffer = new byte[48];

//Set all entries in buffer to zero
for ( int i = 0; i < buffer.length; i++ )
buffer[i] = 0;

//Step 2) Create a datagram socket and open UDP port
DatagramSocket socket = new DatagramSocket();

//Step 3) Send the 4 byte request packet to port 14917
InetAddress address = InetAddress.getByName(args[0]);
DatagramPacket packet = new DatagramPacket(buffer, 4, address, 14917);
socket.send(packet);

System.out.println( "Sending Packet to: "+ args[0] );

//Step 4) Get the response packet from the PVM1010
packet = new DatagramPacket(buffer, buffer.length);
socket.receive(packet);
System.out.println( "Response Packet size: "+ packet.getLength() );

//Step 5) Apply scale factors (for PVP 4800)
int words[] = new int[18];
buffer = packet.getData();
socket.close();

//Print output from the response packet scaled to PVP 4800
//Fill my words array
for ( int w = 0; w < words.length; w++ ) {
  words[w] = getWord( buffer, w );
  System.out.println( " Word " + w + " : " + words[w] );
}

//Arrays.stream(words).forEach(System.outrintln); // !!! THAT SHOULD SAY println, not an emoji !!!
System.out.println( "Inverter Information" );

//Inverter's Type
System.out.println( " Type: "+ words[13] );

//Print out the inverter's status
if ( words[2] == 0x15 )
System.out.println(" Status: Online");
else if ( words[2] == 0x80 )
System.out.println(" Status: Faulted");
else
System.out.println(" Status: Sleeping");

//Print the DC voltage
double dcV = words[5] * 0.015441895 ;
System.out.format( " DC Voltage %.0f V %n", dcV );

//Print the AC voltage
double acV = words[8] * 0.018493652 ;
System.out.format( " AC Voltage %.0f V %n", acV );

//Print the AC Power
double acP = words[4] * 0.393005371 ;
if ( words[2] == 0x15 )
System.out.format( " AC Power: %.0f W %n", acP );

//Print the Temperature
double degC = words[7] * 0.01 ;
double degF = degC * 1.8 + 32 ;
System.out.format( " Temperature: %.0f F %n", degF );
//System.out.format( " Temperature: %.0f C %.0f F %n", degC, degF );


//Print the total kWh
System.out.println( " Total kWh: "+ ((words[16] << 16) + words[17]) );
}

// Get a single word from a buffer
static int getWord( byte buffer[], int word )
{
int word2 = word * 2;

//Make sure word is valid
if ( word2 + 1 > buffer.length || word2 < 0 )
return 0;

//Return the requested word
return ((buffer[word2 + 1] & 255) << 8) +
(buffer[word2 + 0] & 255);
}
}
