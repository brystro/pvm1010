#!/usr/bin/env python3
# This program was adapted from the Java example in
# "UPD Local Communication Interface for the PVM1010.pdf"

import sys
import socket    
import json
import csv
from paho.mqtt import client as mqtt_client
from pathlib import Path
import datetime
import time
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import argparse
import os

# Require Python 3
if sys.version_info[0] < 3 :
    print("This script requires Python 3.x")
    quit()

## Variables ############################################################################

# PVM settings [Required]
pvmhost = 'pvm1010'        # set this to your card's hostname or IP address
pvmport = 14917     # set this to your card's port [14917]
sys_watts = 2500    # set max watts your inverter model supports

# MQTT settings
mqtthost = 'mqtt'            # set MQTT broker hostname or IP
mqttport = 1883              # set MQTT broker port [1883 is default]
mqttuser = 'iot'                # set MQTT broker username
mqttpass = 'Shaff3r577!!'                # set MQTT broker password
topic = 'tele/solar/SENSOR'  # set the MQTT topic, i.e 'tele/solar/SENSOR'
client_id = 'solar'          # set an identifier for this client, i.e 'solar-meter'

# InfluxDB 2 settings
bucket = ''   # set InfluxDB hostname or IP
org = ''      # set InfluxDB organization
token = ''    # set InfluxDB API token for the bucket
url = ''      # set InfluxDB url (include the port, i.e 'https://myinfluxdb.mydomain.com:8086')

# Output settings
output_type = 'mqtt'      # output types: csv, json, mqtt, debug, raw, influxdb
output_filename = ''

## End of Variables #####################################################################

## CLI Parameters
def get_args():
  parser = argparse.ArgumentParser(description='PVM-1010 data collector')
  parser.add_argument('-t', '--type', action='store', choices=['csv', 'json', 'mqtt', 'debug', 'raw', 'influxdb'], help='output types')
  parser.add_argument('-p', '--mqttpass', action='store', help='the MQTT broker password')
  parser.add_argument('-f', '--filename', action='store', help='name of file to write to')

  args = parser.parse_args()
  return args

# Data collection function
def getUDPData(pvmhost, pvmport):

  # create the request packet
  buffer = bytearray(48) 
  for i in range(0, len(buffer)):
    buffer[i] = 0
  
  # ready the connection
  s = socket.socket()
  bufferSize = 1024

   # create socket and open UDP pvmport
  clientsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 

  # send the 4 byte request packet
  clientsocket.sendto(buffer, socket.MSG_WAITALL, (pvmhost, pvmport)) 

  # get the response packet
  data, pvmhost = clientsocket.recvfrom(bufferSize)
  return data

# Data decoding function
def getWord(buffer, word):
   word2 = word * 2
   if (word2 + 1 > len(buffer) or word2 < 0):
    return 0
   return ((buffer[word2 + 1] & 255) << 8) + (buffer[word2 + 0] & 255)

def on_connect(client, userdata, flags, rc):
  if rc == 0:
    client.connected_flag=True
    print("Connected to MQTT mqtthost!")
  else:
    print(f"Failed to connect to {mqtthost}, return code {rc}")
    print("Make sure username and/or password is set correctly")
    client.disconnect()
    return int(rc)

# MQTT output
def output_mqtt(data, args):
  if (mqtthost == '' or mqttuser == '' or topic == '' or client_id == ''):
    print("Please set all mqtt variables (mqtthost, mqttpass, mqttuser and topic)")
    quit()

  if not(args.mqttpass) and not(mqttpass):
    print("Password required for MQTT")
    quit()

  client = mqtt_client.Client(client_id)
  client.username_pw_set(mqttuser, (mqttpass or args.mqttpass))
  client.on_connect = on_connect
  client.connect(mqtthost, mqttport)
  client.loop_start()
  time.sleep(1)
  msg = json.dumps(data)
  result = client.publish(topic, msg, retain=True)
  status = result[0]
  if status == 0:
    print(f"Sent `{msg}` to topic `{topic}`")
  else:
    print(f"Failed to send message to topic {topic}")
  client.disconnect()

# InfluxDB output
def output_influxdb(data):
  if (bucket == '' or org == '' or token == '' or url == ''):
    print("Please set all influxdb variables (bucket, org, token, url)")
    quit()
  
  ifclient = influxdb_client.InfluxDBClient(url, token, org)
  write_api = ifclient.write_api(write_options=SYNCHRONOUS)

  for key, value in data.items():
    # filter out status and timestamp from data object
    if (key == "Status" or key == "Time"):
      next
    else:
      p = influxdb_client.Point(key).field(key, value)
      write_api.write(bucket=bucket, org=org, record=p)
  return

def check_file(filename):
  chkfile = Path(filename)
  if chkfile.is_file():
    return 'a'
  else:
    return 'w'

# JSON output
def output_json(filename, data):
  if(filename):
    file_mode = check_file(filename)
    f = open(filename, file_mode)
    f.write(json.dumps(data, indent=4) + ',\n')
    f.close()
    print('Output written to ' + filename)
  else:
    print(json.dumps(data, indent=4))
  return

# Debug output
def output_debug(raw_data):
  words = [0] * 18
  for w in range(0, len(words)):
    words[w] = getWord(raw_data, w)
  print('received {!r}'.format(raw_data))
  print("Word 0 - Echo :", words[0])
  print("Word 1 - Reserved :", words[1])
  print("Word 2 - State :", words[2])
  print("Word 3 - Fault Code2 :", words[3])
  print("Word 4 - AC Power (unscaled) :", words[4])
  print("Word 5 - DC Voltage (unscaled) :", words[5])
  print("Word 6 - AC Current (unscaled) :", words[6])
  print("Word 7 - Temperature C :", words[7])
  print("Word 8 - AC Voltage (unscaled) :", words[8])
  print("Word 9 - Empty :", words[9])
  print("Word 10 - Inverter Board serial number :", words[10])
  print("Word 11 - Inverter Board type :", words[11])
  print("Word 12 - Firmware version (first byte is major, second is minor) :", words[12])
  print("Word 13 - Inverter Model Type :", words[13])
  print("Word 14 - Inverter Model Serial Number (High Word) :", words[14])
  print("Word 15 - Inverter Model Serial Number (Low Word) :", words[15])
  print("Word 16 - Total kWh (High Word) :", words[16])
  print("Word 17 - Total kWh (Low Word) :", words[17])
  return

# CSV output
def output_csv(filename, data):
  if(not(filename)):
    file_mode = ''
    data_file = sys.stdout
    count = 0
    file_mode = 'w'
  else:
    file_mode = check_file(filename)
    data_file = open(filename, file_mode, newline='')
    print('Output written to ' + filename)

  csv_writer = csv.writer(data_file)
  count = 0

  if(count == 0 and file_mode == 'w'):
    header = data.keys()
    csv_writer.writerow(header)
    count += 1

  csv_writer.writerow(data.values())
  return

# Data output function
def set_output(output_type, filename, args):
  
  # Get the data, format it and set the vars
  raw_data = getUDPData(pvmhost, pvmport)
  words = [0] * 18

  for w in range(0, len(words)):
    words[w] = getWord(raw_data, w)

  # Use the model number ID and get the power factors from the json file
  #pffile = Path('./power_factor.json')
  #print(pffile.resolve())

  ptfile = Path(sys.argv[0])
  pffile = ptfile.with_name('power_factor.json')
  #pffile = str(ptfile.cwd()) + '/power_factor.json'

  try:
    #with open('power_factor.json', 'r') as read_file:
    with open(pffile, 'r') as read_file:
      pf = json.load(read_file)
      pvmDict = pf[str(words[13])]
  except:
    print("Missing power_factor.json file, cannot continue")
    quit()

  status = ""

  if (words[2] == 0x15):
    status = 'online'
    ac_watts = words[4] * pvmDict['AC_WATTS']
  elif (words[2] == 0x80):
    status = 'faulted'
    ac_watts = 0.00
  else:
    status = 'sleeping'
    ac_watts = 0.00

  #if ac_watts > sys_watts: ac_watts = 0.00
  #if ac_watts < 0.00: ac_watts = 0.00

  pv_inverter = pvmDict['MODEL']
  fault_code = words[3]
  dc_volts = words[5] * pvmDict['DC_VOLTS'] 
  ac_volts = words[8] * pvmDict['AC_VOLTS']
  ac_amps = words[6] * pvmDict['AC_AMPS']
  degrees_c = words[7] * 0.01 
  degrees_f = degrees_c * 1.8 + 32
  total_kwh = ((words[16] << 16) + words[17])

# Format the data in a json object
  data = \
    '{"Time":"' + str(datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")) \
    + '","Status":"' + status.capitalize() \
    + '","DC_Volts":' + str(round(dc_volts, 2)) \
    + ',"AC_Volts":' + str(round(ac_volts, 2)) \
    + ',"AC_Power":' + str(round(ac_watts, 2)) \
    + ',"AC_Current":' + str(round(ac_amps, 2)) \
    + ',"Temp_F":' + str(round(degrees_f, 1)) \
    + ',"Temp_C":' + str(round(degrees_c, 1)) \
    + ',"Total_kWh":' + str(total_kwh) \
    + '}'

  data = json.loads(data)

  if output_type == 'csv' or args.type == 'csv':
    output_csv((filename or args.filename), data)
    return

  elif output_type == 'debug' or args.type == 'debug' or args.type == 'raw' or output_type == 'raw':
    output_debug(raw_data)
    return

  elif output_type == 'json' or args.type == 'json':
    output_json((filename or args.filename), data)
    return

  elif output_type == 'mqtt' or args.type == 'mqtt':
    output_mqtt(data, args)
    return
  
  elif output_type == 'influxdb' or args.type == 'influxdb':
    output_influxdb(data)
    return

  else:
    print("Inverter Information")
    print("Model:", pv_inverter)
    print("Status:", status.capitalize())
    print("Fault Code:", fault_code)
    print('DC Voltage: %.1f V' % dc_volts)
    print('AC Voltage: %.1f V' % ac_volts)
    print('AC Power: %.1f Watts' % ac_watts)
    print('AC Current: %.1f Amps' % ac_amps)
    print("Temperature: %.1f F" % degrees_f)
    print("Total kWh:", ((words[16] << 16) + words[17]))
    print("")
    return

def main():
  args = get_args()
  if not(pvmhost):
    print("PVM hostname or IP address is required")
    quit()

  set_output(output_type, output_filename, args)

if __name__ == "__main__":
  main()
