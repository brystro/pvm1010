# PVM1010
This is a Python script to gather data from the PVPowered PVM-1010 module.  
This script was adapted from the Java example in the [PVM1010 UDP Local Communication Interface.pdf](PVM1010 UDP Local Communication Interface.pdf) contained in this repository.

## Usage
./pvm1010.py  

This script **requires** python 3.x or higher  
The variables must be set in the pvm1010.py script itself.

## Variables
#### PVM settings [Required]
**pvmhost** - Set this to your card's hostname or IP address   
**pvmport** - Set this to your card's port [14917]

#### MQTT settings
**mqtthost** - Set to the MQTT broker hostname or IP  
**mqttport** - Set to the MQTT broker port [1883 is default]  
**mqttuser** - Set to the MQTT broker username  
**mqttpass** - Set to the MQTT broker password  
**topic** - Set this to the MQTT topic (must be quoted), i.e 'tele/solar/SENSOR'  
**client_id** - Set this to the MQTT client name  

#### Output settings
**output_type** - Set this to output type: **csv, json, mqtt, debug, raw**  
**output_filename** - Set this to the filename to output, use full path (quoted) or file will be created in location the script runs from

## Power Factoring
Power factoring is included by default.  You do not need to edit the code to set the correct factoring as the script will detect the inverter type and apply the values automatically.  The information is pulled from the power_factor_table.json file.  Do not delete this file or the script will not work.

## Sourcing a PVM-1010
These modules are hard to find, but I was searching eBay and took a chance on a CSM-1010 module which is the exact same thing as a PVM-1010.
![ebay-csm1010](ebay-csm1010.png)
## Configuring Your PVM-1010
You must know the IP address of your PVM-1010.  By default it is shipped to used DHCP.  You can use the PVMSync application (Windows only) included in this repo to locate your module.  

#### Assigning a static IP
Contrary to what the application says is optional, you **MUST** assign at least one DNS server.  Use the PVMSync application (Windows only) included in this repo.

### Acknowledgments
Thanks to mjburns from the SolarPanelTalk forum for posting the PVMSync program.   
https://www.solarpaneltalk.com/forum/solar-panels-for-home/solar-panel-system-equipment/420643-software-for-monitoring-pvp4800-with-pvm1010
